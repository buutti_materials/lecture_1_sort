package com.buutcamp.springdemo.Config;


import com.buutcamp.springdemo.implementation.BubbleSort;
import com.buutcamp.springdemo.implementation.InsertionSort;
import com.buutcamp.springdemo.interfaces.SortingAlgorithm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Conf {

    @Bean
    public SortingAlgorithm bubbleSort() {
        return new BubbleSort();
    }

    @Bean
    public SortingAlgorithm insertionSort() {
        return new InsertionSort();
    }



}
