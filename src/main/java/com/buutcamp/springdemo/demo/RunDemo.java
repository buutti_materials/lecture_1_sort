package com.buutcamp.springdemo.demo;

import com.buutcamp.springdemo.Config.Conf;
import com.buutcamp.springdemo.interfaces.SortingAlgorithm;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class RunDemo {

    public static void main(String[] args) {

        int[] numbers = {2,3,6,1,3,2,8,6,2,3,4,9,4,12,61,23,43};


        //Get context
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Conf.class);

        //Get Bean from context
        SortingAlgorithm b = context.getBean("bubbleSort", SortingAlgorithm.class);

        int[] n = b.sort(numbers);

        for (int N : n) {
            System.out.println(N);
        }

    }

}
