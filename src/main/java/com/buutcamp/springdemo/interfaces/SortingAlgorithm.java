package com.buutcamp.springdemo.interfaces;

public interface SortingAlgorithm {

    int[] sort(int[] numbers);
}
